/*jshint node:true*/
"use strict";

var express = require("express");
var router = express.Router();
var models = require("./../models/models");
var List = models.List;
var Task = require("./../models/task");

var error = function(err) {
	console.error(err);
};

router.get("/", function(req, res) {
	console.log(req.method, req.url);

	List.find(function(err, lists) {
		if (err) {
			error(err);
			res.status(500).send();
			return;
		}
		res.json(lists);
		return;
	});

});

router.post("/", function(req, res) {
	console.log(req.method, req.url);

	if (!req.body) {
		error("Missing request body");
		res.status(500).send();
		return;
	}

	if (!req.body.lists) {
		error("Missing request lists");
		res.status(500).send();
		return;
	}

	if (!req.body.lists.tasks) {
		error("Missing tasks");
		res.status(500).send();
	}

	if (!req.body.lists.users) {
		error("Missing user(s)");
		res.status(500).send();
	}

	var list;
	try {
		list = new List();
		list.name = req.body.lists.name;
		list.tasks = req.body.lists.tasks;
	} catch(err) {
		error("Failed to create list");
		error(err);
		res.status(500).send();
		return;
	}

	console.log("Created list in memory", list);

	list.save(function(err) {
		if (err) {
			error("Failed to save list");
			error(err);
			res.status(500).send();
			return;
		} else {
			console.log("Saved list to database");
			res.json(list);
			return;
		}

	});
});

module.exports = router;
