/*jshint node:true*/
"use strict";

var express = require("express");
var router = express.Router();
var models = require("./../models/models");
var List = models.List;
var Task = models.Task;

router.get("/list", function(req, res) {
	console.log(req.method, req.url);

	var list = new List({
		name: "Test list",
		tasks: [
			new Task({body: "Task A"}),
			new Task({body: "Task B"}),
			new Task({
				body: "Task Deleted",
				modified: Date.Now,
				deleted: Date.Now
			}),
			new Task({
				body: "Task Done",
				modified: Date.Now,
				completed: Date.Now
			})
		]
	});

	console.log("TEST LIST");
	console.log(list);

	res.json(list);

});

router.get("/lists", function(req, res) {
	console.log(req.method, req.url);

	var lists = {
		lists: [
			new List({
				name: "Groceries",
				tasks: [
					new Task({body: "Milk"}),
					new Task({body: "Cheeseballs"}),
					new Task({body: "Beers"})
				]}),
			new List({
				name: "Home",
				tasks: [
					new Task({body: "Build a bar"}),
					new Task({body: "Fix the roof"}),
					new Task({
						body: "Hide the treasure",
						modified: Date.Now,
						deleted: Date.Now
					})
				]}),
			new List({
				name: "Projects",
				tasks: [
					new Task({
						body: "Wake up",
						modified: Date.Now,
						completed: Date.Now
					}),
					new Task({body: "Get filthy rich"}),
					new Task({body: "Achieve world domination"}),
				]})
		]
	};

	console.log("TEST LISTS");
	console.log(lists);

	res.json(lists);

});

module.exports = router;