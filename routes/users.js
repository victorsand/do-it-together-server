/*jshint node:true*/

"use strict";

var express = require("express");
var router = express.Router();
var models = require("./../models/models");
var List = models.List;


router.get("/", function(req, res) {
	console.log(req.method, req.url);

	var error = function(err) {
		console.error(err);
	};

	List.find(function(err, lists) {
		if (err) {
			error(err);
			return;
		}
		res.json(lists);
	});

});

router.post("/", function(req, res) {
	console.log(req.method, req.url);

	var error = function(err) {
		console.error(err);
	};

	if (!req.body) {
		error("Missing request body");
		return;
	}

	if (!req.body.lists) {
		error("Missing request lists");
		return;
	}

	if (!req.body.lists.tasks) {
		error("Missing tasks");
	}

	if (!req.body.lists.users) {
		error("Missing user(s)");
	}

	var list = new List();
	list.name = req.body.lists.name;
	list.tasks = req.body.lists.tasks;

	console.log("Created list in memory", list);

	list.save(function(err) {

		if (err) {
			error("Failed to save list");
			return;
		} else {
			console.log("Saved list to database");
			res.json(list);
		}

	});
});

module.exports = router;
