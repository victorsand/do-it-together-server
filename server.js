/*jshint node:true*/

var express = require("express");
var bodyParser = require("body-parser");
var database = require("./config/env/development");
var mongoose = require("mongoose");


var port = process.env.PORT || 8080;

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
	console.log(req.method, req.url);
	next();
});

var lists = require('./routes/lists');
var testlists = require('./routes/testlists');

app.use('/lists', lists);
app.use('/testlists', testlists);

mongoose.connect(database.uri, function(err) {
	if (err) {
		console.error("WARNING: Could not connect to dabatase");
	}
});

app.listen(port);

console.log("Database: ", database);
console.log("Listening on port ", port);