/*jshint node:true*/
"use strict";

module.exports = function(grunt) {

	console.log("Running Grunt");


	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		env: {
			dev: {
				NODE_ENV: "development"
			}
		},
		mochaTest: {
			options: {
				reporter: "spec"
			},
			src: ["test/*.js"]
		}
	});

	grunt.loadNpmTasks("grunt-services");
	grunt.loadNpmTasks("grunt-mocha-test");

	console.log("process.env.NODE_ENV:", process.env.NODE_ENV);
	if (process.env.NODE_ENV === "production") {
		console.log("Registering PRODUCTION tasks");
	} else {
		console.log("Registering DEVELOPMENT tasks");
		grunt.registerTask("start", "Start MongoDB database", ["startMongo"]);
		grunt.registerTask("stop", "Stop MongoDB database", ["stopMongo"]);
		grunt.registerTask("test", ["mochaTest"]);
	}

	grunt.registerTask("default", []);


};