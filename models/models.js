var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
	body: { type: String, required: true },
	created: { type: Date, default: Date.now },
	modified: { type: Date, default: Date.now },
	completed: { type: Date, default: null },
	deleted: { type: Date, default: null }
});
module.exports.Task = mongoose.model('Task', TaskSchema);

var ListSchema = new Schema({
	name: { type: String, required: true },
	tasks: [TaskSchema],
	users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
	created: { type: Date, default: Date.now },
	modified: { type: Date, default: Date.now },
	deleted: { type: Date, default: null }
});
module.exports.List = mongoose.model('List', ListSchema);

var UserSchema = new Schema({
	email: { type: String, required: true },
	name: { type: String, required: true },
	created: { type: Date, default: Date.now },
	modified: { type: Date, default: Date.now },
	deleted: { type: Date, default: null },
	lists: [{ type: Schema.Types.ObjectId, ref: 'List' }]
});
module.exports.User = mongoose.model('User', UserSchema);