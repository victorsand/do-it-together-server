var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var TaskSchema = require("./task").schema;

var ListSchema = new Schema({
	name: { type: String, required: true },
	tasks: [TaskSchema],
	users: [{ type: Schema.Types.ObjectId, ref: "User" }],
	created: { type: Date, default: Date.now },
	modified: { type: Date, default: Date.now },
	deleted: { type: Date, default: null }
});

ListSchema.methods.setName = function(name) {
	console.log("setting list name to '", name, "'");
	this.name = name;
	this.modified = new Date();
};

ListSchema.methods.addUser = function(userId) {
	console.log("adding user", userId);
	this.users.push(userId);
	this.modified = new Date();
};

ListSchema.methods.removeUser = function(userId) {
	console.log("removing user", userId);
	var index = this.users.indexOf(userId);
	if (index > -1) {
		this.users.splice(index, 1);
		this.modified = new Date();
		console.log("removed user");
	} else {
		console.log("user not in list");
	}
};

module.exports = mongoose.model("List", ListSchema);
