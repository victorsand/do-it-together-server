var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
	body: { type: String, required: true },
	created: { type: Date, default: new Date() },
	modified: { type: Date, default: new Date() },
	completed: { type: Date, default: null },
	deleted: { type: Date, default: null }
});

TaskSchema.methods.setBody = function(body) {
	this.body = body;
	this.modified = new Date();
};

TaskSchema.methods.complete = function(body) {
	this.completed = this.modified = new Date();
};

TaskSchema.methods.delete = function() {
	this.deleted = this.modified = new Date();
};

module.exports = mongoose.model('Task', TaskSchema);