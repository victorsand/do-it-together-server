/*jshint node:true*/
/*global describe,beforeEach,afterEach,it*/

"use strict";

var mongoose = require("mongoose");
var ObjectId = mongoose.Types.ObjectId;
var List = require("./../models/list");

var expect = require("expect.js");

describe("List schema and model", function() {

	describe("Schema", function() {

		var list;

		beforeEach(function() {
			list = new List({
				name: "test list"
			});
		});

		it("has a name", function() {
			expect(list.name).to.be("test list");
		});

		it("has a list of tasks", function() {
			expect(list.tasks).to.be.an("array");
			expect(list.tasks).to.be.empty();
		});

		it("has a list of users", function() {
			expect(list.users).to.be.an("array");
			expect(list.users).to.be.empty();
		});

		it("has a created date", function() {
			expect(list.created.getFullYear()).to.be(new Date().getFullYear());
		});

		it("has a modified date", function() {
			expect(list.modified.getFullYear()).to.be(new Date().getFullYear());
		});

		it("has a deleted date", function() {
			expect(list.deleted).to.be(null);
		});

	});

	describe("setName", function() {

		var list;

		beforeEach(function() {
			list = new List({
				name: "test list",
				modified: new Date(2000, 1, 1)
			});
			list.setName("new name");
		});

		it("sets new name", function() {
			expect(list.name).to.be("new name");
		});

		it("updates modified date", function() {
			expect(list.modified.getFullYear()).to.be(new Date().getFullYear());
		});

	});

	describe("addUser", function() {

		var list;
		var userId = new ObjectId();

		beforeEach(function() {
			list = new List({
				name: "test list",
				modified: new Date(2000, 1, 1)
			});
			list.addUser(userId);
		});

		it("adds an object id to user list", function() {
			expect(list.users).to.have.length(1);
			expect(list.users).to.contain(userId);
		});

		it("updates modified date", function() {
			expect(list.modified.getFullYear()).to.be(new Date().getFullYear());
		});

		it("does not remove previous user", function() {
			var anotherUserID = new ObjectId();
			list.addUser(anotherUserID);
			expect(list.users).to.have.length(2);
			expect(list.users).to.contain(userId);
			expect(list.users).to.contain(anotherUserID);
		});

	});

	describe("removeUser", function() {

		var list;
		var userId = new ObjectId();
		var anotherUserID = new ObjectId();

		beforeEach(function() {
			list = new List({
				name: "test list",
				modified: new Date(2000, 1, 1)
			});
		});

		it("removes a user", function() {
			list.addUser(userId);
			list.addUser(anotherUserID);
			list.removeUser(userId);
			expect(list.users).to.have.length(1);
			expect(list.users).to.contain(anotherUserID);
			expect(list.users).to.not.contain(userId);
		});

		it("updates modified date", function() {
			list.addUser(userId);
			list.addUser(anotherUserID);
			list.removeUser(userId);
			expect(list.modified.getFullYear()).to.be(new Date().getFullYear());
		});

		it("does not remove anything if user id does not exist", function() {
			list.addUser(userId);
			list.removeUser(new ObjectId());
			expect(list.users).to.have.length(1);
			expect(list.users).to.contain(userId);
		});

		it("does not update modified date if user id does not exist", function() {
			list.addUser(userId);
			list.modified = new Date(2000, 1, 1);
			list.removeUser(new ObjectId());
			expect(list.modified.getFullYear()).to.be(2000);
		});
	});
});



