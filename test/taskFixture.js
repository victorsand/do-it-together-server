/*jshint node:true*/
/*global describe,beforeEach,afterEach,it*/
"use strict";

var Task = require("./../models/task");
var expect = require("expect.js");

describe("Task schema and model", function() {

	describe("Schema", function() {

		var task = null;

		beforeEach(function() {
			task = new Task({
				body: "task body"
			});
		});

		it("has a body", function() {
			expect(task.body).to.eql("task body");
		});

		it("has a created date", function() {
			expect(task.created.getFullYear()).to.eql(new Date().getFullYear());
		});

		it("has a modified date", function() {
			expect(task.modified.getFullYear()).to.eql(new Date().getFullYear());
		});

		it("has a completed date set to null", function() {
			expect(task.completed).to.eql(null);
		});

		it("has a deleted date set to null", function() {
			expect(task.deleted).to.eql(null);
		});

	});

	describe("setBody", function() {

		var task = null;

		beforeEach(function() {
			task = new Task({
				body: "task body",
				modified: new Date(2000, 1, 1)
			});
			expect(task.modified.getFullYear()).to.not.eql(new Date().getFullYear());
			task.setBody("new body");
		});

		it("sets body", function() {
			expect(task.body).to.eql("new body");
		});

		it("updates modified date", function() {
			expect(task.modified.getFullYear()).to.eql(new Date().getFullYear());
		});

	});

	describe("complete", function() {

		var task = null;

		beforeEach(function() {
			task = new Task({
				modified: new Date(2000, 1, 1)
			});
			expect(task.modified.getFullYear()).to.not.eql(new Date().getFullYear());
			expect(task.completed).to.eql(null);
			task.complete();
		});

		it("sets complete date", function() {
			expect(task.completed.getFullYear()).to.eql(new Date().getFullYear());
		});

		it("updates modified date", function() {
			expect(task.modified.getFullYear()).to.eql(new Date().getFullYear());
		});

	});

	describe("delete", function() {

		var task = null;

		beforeEach(function() {
			task = new Task({
				modified: new Date(2000, 1, 1)
			});
			expect(task.modified.getFullYear()).to.not.eql(new Date().getFullYear());
			expect(task.deleted).to.eql(null);
			task.delete();
		});

		it("sets deleted date", function() {
			expect(task.deleted.getFullYear()).to.eql(new Date().getFullYear());
		});

		it("updates modified date", function() {
			expect(task.modified.getFullYear()).to.eql(new Date().getFullYear());
		});


	});

});